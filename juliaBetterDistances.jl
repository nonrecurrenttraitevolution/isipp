### use this code to generate pairwise distances of all rows of csv phylogenetic data

using CSV
using DataFrames 
using Statistics

allEvo = CSV.read("allEvoOutput2.csv", DataFrame; header = false);
h = CSV.read("hawaiiEvoOutput2.csv", DataFrame; header = false);
k = CSV.read("kauaiEvoOutput2.csv", DataFrame; header = false);
kh = CSV.read("kauaiHawaiiEvoOutput2.csv", DataFrame; header = false);
km = CSV.read("kauaiMauiEvoOutput2.csv", DataFrame; header = false);
ko = CSV.read("kauaiOahuEvoOutput2.csv", DataFrame; header = false);
kom = CSV.read("kauaiOahuMauiEvoOutput2.csv", DataFrame; header = false);
m = CSV.read("mauiEvoOutput2.csv", DataFrame; header = false);
mh = CSV.read("mauiHawaiiEvoOutput2.csv", DataFrame; header = false);
o = CSV.read("oahuEvoOutput2.csv", DataFrame; header = false);
oh = CSV.read("oahuHawaiiEvoOutput2.csv", DataFrame; header = false);
om = CSV.read("oahuMauiEvoOutput2.csv", DataFrame; header = false);
omh = CSV.read("oahuMauiHawaiiEvoOutput2.csv", DataFrame; header = false);

# transform all transposed matrices, so calculations go faster downstream

allM = Matrix(allEvo)';
hM = Matrix(h)';
kM = Matrix(k)';
khM = Matrix(kh)';
kmM = Matrix(km)';
koM = Matrix(ko)';
komM = Matrix(kom)';
mM = Matrix(m)';
mhM = Matrix(mh)';
oM = Matrix(o)';
ohM = Matrix(oh)';
omM = Matrix(om)';
omhM = Matrix(omh)'; 


### code from https://discourse.julialang.org/u/rdeits on juliahub 
### see https://discourse.julialang.org/t/improving-performance-of-a-nested-for-loop/29705/7 
# run on "c_cols" ; translated matrix works much better


function triangle_number(n)
    if iseven(n)
        (n ÷ 2) * (n + 1)
    else
        ((n + 1) ÷ 2) * n
    end
end

function distance_matrix4(c)
    if sum(c) == 0
        cdist = c
    else    
        cdist = Matrix{eltype(c)}(undef, size(c, 1), triangle_number(size(c, 2) - 1))
        col = 1
        for j in 1:(size(c, 2) - 1)
            for h in (j + 1):size(c, 2)
                for k in 1:size(c, 1)
                    cdist[k, col] = abs(c[k, j] - c[k, h])
                end
                col += 1
            end
        end
        return cdist
    end
end



### note that below the matrices are transposed before they're read into the _dist.csv files

allM_dist = distance_matrix4(allM)'; 
hM_dist = distance_matrix4(hM)'; 
kM_dist = distance_matrix4(kM)'; 
khM_dist = distance_matrix4(khM)'; 
kmM_dist = distance_matrix4(kmM)'; 
koM_dist = distance_matrix4(koM)'; 
komM_dist = distance_matrix4(komM)'; 
mM_dist = distance_matrix4(mM)'; 
mhM_dist = distance_matrix4(mhM)'; 
oM_dist = distance_matrix4(oM)'; 
ohM_dist = distance_matrix4(ohM)'; 
omM_dist = distance_matrix4(omM)'; 
omhM_dist = distance_matrix4(omhM)'; 

# write out files

CSV.write("all_dists.csv", DataFrame(allM_dist, :auto), header = false);
CSV.write("h_dists.csv", DataFrame(hM_dist, :auto), header = false);
CSV.write("k_dists.csv", DataFrame(kM_dist, :auto), header = false);
CSV.write("kh_dists.csv", DataFrame(khM_dist, :auto), header = false);
CSV.write("km_dists.csv", DataFrame(kmM_dist, :auto), header = false);
CSV.write("ko_dists.csv", DataFrame(koM_dist, :auto), header = false);
CSV.write("kom_dists.csv", DataFrame(komM_dist, :auto), header = false);
CSV.write("m_dists.csv", DataFrame(mM_dist, :auto), header = false);
CSV.write("mh_dists.csv", DataFrame(mhM_dist, :auto), header = false);
CSV.write("o_dists.csv", DataFrame(oM_dist, :auto), header = false);
CSV.write("oh_dists.csv", DataFrame(ohM_dist, :auto), header = false);
CSV.write("om_dists.csv", DataFrame(omM_dist, :auto), header = false);
CSV.write("omh_dists.csv", DataFrame(omhM_dist, :auto), header = false);

# dance for joy 


### function for comparing two matrices (test for monophyly)


function distance_matrix5(C, D)
    if sum(C) == 0
        CDdist = C
    elseif sum(D) == 0
        CDdist = D
    else    
        CDdist = Matrix{eltype(C)}(undef, size(C, 1), size(C, 2)*size(D, 2))
        col = 1
        for j in 1:size(C, 2)
            for h in 1:size(D, 2)
                for k in 1:size(C, 1)
                    CDdist[k, col] = abs(C[k, j] - D[k, h])
                end
                col += 1
            end
        end
        return CDdist
    end
end

HoutM_dist = distance_matrix5(hM, mM)';
MoutO_dist = distance_matrix5(mM, oM)';
OoutK_dist = distance_matrix5(oM, kM)';
HoutO_dist = distance_matrix5(hM, oM)';
HoutK_dist = distance_matrix5(hM, kM)';
MoutK_dist = distance_matrix5(mM, kM)';

### write these files, too: 
CSV.write("HoutM_dists.csv", DataFrame(HoutM_dist, :auto), header = false);
CSV.write("MoutO_dists.csv", DataFrame(MoutO_dist, :auto), header = false);
CSV.write("OoutK_dists.csv", DataFrame(OoutK_dist, :auto), header = false);
CSV.write("HoutO_dists.csv", DataFrame(HoutO_dist, :auto), header = false);
CSV.write("HoutK_dists.csv", DataFrame(HoutK_dist, :auto), header = false);
CSV.write("MoutK_dists.csv", DataFrame(MoutK_dist, :auto), header = false);

### make sure you have a .sh script in your working directory 
### might need to change "zsh" to an appropriate shell interpreter (e.g., "sh")
run(`zsh finalTextProcessing.zsh`)

### make sure to take out the "remove line that contains 'missing'" things into the bash script

allDist = Matrix(CSV.read("all_dists.csv.done.csv", DataFrame; header = false));
hDist = Matrix(CSV.read("h_dists.csv.done.csv", DataFrame; header = false));
kDist = Matrix(CSV.read("k_dists.csv.done.csv", DataFrame; header = false));
khDist = Matrix(CSV.read("kh_dists.csv.done.csv", DataFrame; header = false));
kmDist = Matrix(CSV.read("km_dists.csv.done.csv", DataFrame; header = false));
koDist = Matrix(CSV.read("ko_dists.csv.done.csv", DataFrame; header = false));
komDist = Matrix(CSV.read("kom_dists.csv.done.csv", DataFrame; header = false));
mDist = Matrix(CSV.read("m_dists.csv.done.csv", DataFrame; header = false));
mhDist = Matrix(CSV.read("mh_dists.csv.done.csv", DataFrame; header = false));
oDist = Matrix(CSV.read("o_dists.csv.done.csv", DataFrame; header = false));
ohDist = Matrix(CSV.read("oh_dists.csv.done.csv", DataFrame; header = false));
omDist = Matrix(CSV.read("om_dists.csv.done.csv", DataFrame; header = false));
omhDist = Matrix(CSV.read("omh_dists.csv.done.csv", DataFrame; header = false));
HoutM_dist =  Matrix(CSV.read("HoutM_dists.csv.done.csv", DataFrame; header = false));
MoutO_dist = Matrix(CSV.read("MoutO_dists.csv.done.csv", DataFrame; header = false));
OoutK_dist = Matrix(CSV.read("OoutK_dists.csv.done.csv", DataFrame; header = false));
HoutO_dist = Matrix(CSV.read("HoutO_dists.csv.done.csv", DataFrame; header = false));
HoutK_dist = Matrix(CSV.read("HoutK_dists.csv.done.csv", DataFrame; header = false));
MoutK_dist = Matrix(CSV.read("MoutK_dists.csv.done.csv", DataFrame; header = false));

### read in seeds value so I can associate these data with the R output
theseSeeds = CSV.read("seeds.csv", DataFrame; header = true);


### the files that were only zeros will just be six lines of whatever the evoTime number was (length of the rows)
### to make those easier to remove downstream (while still allowing the summary stats to work below) just change them to Inf

if allDist[1] == sum(allDist)/size(allDist)[1]
	allDist = [Inf]
else
	allDist = allDist;
end		



if hDist[1] == sum(hDist)/size(hDist)[1]
	hDist = [Inf]
else
	hDist = hDist;
end		



if kDist[1] == sum(kDist)/size(kDist)[1]
	kDist = [Inf]
else
	kDist = kDist;
end		


if khDist[1] == sum(khDist)/size(khDist)[1]
	khDist = [Inf]
else
	khDist = khDist;
end		


if kmDist[1] == sum(kmDist)/size(kmDist)[1]
	kmDist = [Inf]
else
	kmDist = kmDist;
end		


if koDist[1] == sum(koDist)/size(koDist)[1]
	koDist = [Inf]
else
	koDist = koDist;
end		


if komDist[1] == sum(komDist)/size(komDist)[1]
	komDist = [Inf]
else
	komDist = komDist;
end		


if mDist[1] == sum(mDist)/size(mDist)[1]
	mDist = [Inf]
else
	mDist = mDist;
end		


if mhDist[1] == sum(mhDist)/size(mhDist)[1]
	mhDist = [Inf]
else
	mhDist = mhDist;
end		


if oDist[1] == sum(oDist)/size(oDist)[1]
	oDist = [Inf]
else
	oDist = oDist;
end		


if ohDist[1] == sum(ohDist)/size(ohDist)[1]
	ohDist = [Inf]
else
	ohDist = ohDist;
end		


if omDist[1] == sum(omDist)/size(omDist)[1]
	omDist = [Inf]
else
	omDist = omDist;
end		


if omhDist[1] == sum(omhDist)/size(omhDist)[1]
	omhDist = [Inf]
else
	omhDist = omhDist;
end		


if HoutM_dist[1] == sum(HoutM_dist)/size(HoutM_dist)[1]
	HoutM_dist = [Inf]
else
	HoutM_dist = HoutM_dist;
end		


if MoutO_dist[1] == sum(MoutO_dist)/size(MoutO_dist)[1]
	MoutO_dist = [Inf]
else
	MoutO_dist = MoutO_dist;
end		


if hDist[1] == sum(hDist)/size(hDist)[1]
	hDist = [Inf]
else
	hDist = hDist;
end		


if OoutK_dist[1] == sum(OoutK_dist)/size(OoutK_dist)[1]
	OoutK_dist = [Inf]
else
	OoutK_dist = OoutK_dist;
end		


if HoutO_dist[1] == sum(HoutO_dist)/size(HoutO_dist)[1]
	HoutO_dist = [Inf]
else
	HoutO_dist = HoutO_dist;
end		


if HoutK_dist[1] == sum(HoutK_dist)/size(HoutK_dist)[1]
	HoutK_dist = [Inf]
else
	HoutK_dist = HoutK_dist;
end		


if MoutK_dist[1] == sum(MoutK_dist)/size(MoutK_dist)[1]
	MoutK_dist = [Inf]
else
	MoutK_dist = MoutK_dist;
end		


output = [theseSeeds[1 , 1] theseSeeds[1 , 2] median(allDist) minimum(allDist) maximum(allDist) count(x -> x < 4, allDist) median(hDist) minimum(hDist) maximum(hDist) count(x -> x < 4, hDist) median(kDist) minimum(kDist) maximum(kDist) count(x -> x < 4, kDist) median(khDist) minimum(khDist) maximum(khDist) count(x -> x < 4, khDist) median(kmDist) minimum(kmDist) maximum(kmDist) count(x -> x < 4, kmDist) median(koDist) minimum(koDist) maximum(koDist) count(x -> x < 4, koDist) median(komDist) minimum(komDist) maximum(komDist) count(x -> x < 4, komDist) median(mDist) minimum(mDist) maximum(mDist) count(x -> x < 4, mDist) median(mhDist) minimum(mhDist) maximum(mhDist) count(x -> x < 4, mhDist) median(oDist) minimum(oDist) maximum(oDist) count(x -> x < 4, oDist) median(ohDist) minimum(ohDist) maximum(ohDist) count(x -> x < 4, ohDist) median(omDist) minimum(omDist) maximum(omDist) count(x -> x < 4, omDist) median(omhDist) minimum(omhDist) maximum(omhDist) count(x -> x < 4, omhDist) median(HoutM_dist) minimum(HoutM_dist) maximum(HoutM_dist) count(x -> x < 4, HoutM_dist) median(MoutO_dist) minimum(MoutO_dist) maximum(MoutO_dist) count(x -> x < 4, MoutO_dist) median(OoutK_dist) minimum(OoutK_dist) maximum(OoutK_dist) count(x -> x < 4, OoutK_dist) median(HoutO_dist) minimum(HoutO_dist) maximum(HoutO_dist) count(x -> x < 4, HoutO_dist) median(HoutK_dist) minimum(HoutK_dist) maximum(HoutK_dist) count(x -> x < 4, HoutK_dist) median(MoutK_dist) minimum(MoutK_dist) maximum(MoutK_dist) count(x -> x < 4, MoutK_dist)];



### julia won't let me export with line breaks, so I gotta do it as I do above
### also Int won't work because I have some outputs as Inf sometimes 
#output = Int[ theseSeeds[1 , 1] theseSeeds[1 , 2] 
#	median(allDist) minimum(allDist) maximum(allDist) count(x -> x < 4, allDist) 
#	median(hDist) minimum(hDist) maximum(hDist) count(x -> x < 4, hDist) 
#	median(kDist) minimum(kDist) maximum(kDist) count(x -> x < 4, kDist) 
#	median(khDist) minimum(khDist) maximum(khDist) count(x -> x < 4, khDist) 
#	median(kmDist) minimum(kmDist) maximum(kmDist) count(x -> x < 4, kmDist) 
#	median(komDist) minimum(komDist) maximum(komDist) count(x -> x < 4, komDist) 
#	median(mDist) minimum(mDist) maximum(mDist) count(x -> x < 4, mDist) 
#	median(mhDist) minimum(mhDist) maximum(mhDist) count(x -> x < 4, mhDist) 
#	median(oDist) minimum(oDist) maximum(oDist) count(x -> x < 4, oDist) 
#	median(ohDist) minimum(ohDist) maximum(ohDist) count(x -> x < 4, ohDist) 
#	median(omDist) minimum(omDist) maximum(omDist) count(x -> x < 4, omDist) 
#	median(omhDist) minimum(omhDist) maximum(omhDist) count(x -> x < 4, omhDist) 
#	median(HoutM_dist) minimum(HoutM_dist) maximum(HoutM_dist) count(x -> x < 4, HoutM_dist)
#	median(MoutO_dist) minimum(MoutO_dist) maximum(MoutO_dist) count(x -> x < 4, MoutO_dist)
#	median(OoutK_dist) minimum(OoutK_dist) maximum(OoutK_dist) count(x -> x < 4, OoutK_dist)
#	median(HoutO_dist) minimum(HoutO_dist) maximum(HoutO_dist) count(x -> x < 4, HoutO_dist)
#	median(HoutK_dist) minimum(HoutK_dist) maximum(HoutK_dist) count(x -> x < 4, HoutK_dist)
#	median(MoutK_dist) minimum(MoutK_dist) maximum(MoutK_dist) count(x -> x < 4, MoutK_dist)
#	]

CSV.write("ISIPPjuliaOutput.csv", DataFrame(output, :auto), append=true)

