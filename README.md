**Getting things running**

Basically, four separate files to think about: 

the .R script that runs the bulk of the simulation

two .jl (julia language) files that partially process the phylogenetic output from the simulation (juliaSummaryStatisticsOutput.jl & julia_csvrowsPairwiseDistances.jl)

some unix code to do some basic text processing of the output, and to script it all together and potentially run lots of simulations ( "terminal unix code for scripting simulation" )

the unix is mostly things like "sed", "tail", "tr"
it's been tested in the MacOS terminal environment (zsh)

it relies on being able to call R and Julia, so you'll need to make sure the command "Rscript" calls R, and set up a shortcut to call Julia (I'm using the call "julia")

Each time I open a new terminal window, I've been running: 
alias julia="/Applications/Julia-1.4.app/Contents/Resources/julia/bin/julia" 

...but there might be a more permanent way of doing this

The R code doesn't require any extra libraries, but Julia does. Each time you update Julia you'll have to re-add the following packages: 
Statistics
CSV
DataFrames

as of May 2020, I've run this code with Julia 1.2 - 1.4 and it seems to have worked the same each time. 


**Wrangling the data**
The simulations will generate lots of output files, many of which are a way of storing the phylogenetic information from different combinations of islands (k = Kauai; o = Oahu; m = Maui; h = Hawaii; these are the oldest to youngest islands in sequence)


The main output files that will be useful are "ISIPPparameterSpace.csv" which has all of the output from the R code, including the initially generated parameters and the final species richness values for all the islands

"ISIPPjuliaOutput.csv" has the maximum and median phylogenetic distances from different combinations of islands. This is ultimately what one might want to test the generality of the island progression pattern

For each simulation run "thisSeed" and "notThisSeed" are associated with the data from that simulation, so can be used to associate run data among the four different files below. 

The other two output files are "ISIPP_preRun_parameterSpace.csv" and "ISIPP_stepOutput.csv"
The preRun_parameterSpace file only has the parameter space from the simulations, and not the diversity output. This might be useful if a run fails, or needs to be terminated because it starts generating ridiculous levels of diversity. 
The stepOutput file contains the species richness data for each time step of each simulation (associated with "thisSeed" and "notThisSeed")
This might be cool if you're looking for evidence of equilibrial diversity dynamics, or looking at how many time steps occur between island formation and island colonization at different island distances. 

Finally, to really look at the data, I generally post-process in R (including a few steps that take advantage of the "tibble" and "dplyr" libraries). Please see one of the "postProcessingInR" files for more info. 
If you are varying many parameters, and running many many simulations, it might be useful to explore the data with the CART (classification and regression trees) implemented in the library rpart. 
If you're more interested in just a few variables at a time, linear models, glm, or other statistical approaches that come with base R might be more appropriate. 

If you might want to visualize a nonlinear relationship, I like the LOESS implementation in the library "msir"


**Further info**
For a bit more of an in-depth discussion of the code, check out
https://gitlab.com/nonrecurrenttraitevolution/isipp/wikis/In-silico-Island-Progression-Rule-Simulations
