#!/bin/zsh

ls *_dists.csv | while read x; do \
   sed -e 's/".*"//' $x | tr -d ',' | sed 's/[^1]*\(1.*\)/\1/' | awk '{ print length($0) }' > "$x.done.csv"  ;
done

