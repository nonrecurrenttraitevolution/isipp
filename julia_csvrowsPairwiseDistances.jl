### use this code to generate pairwise distances of all rows of csv phylogenetic data

using CSV
using DataFrames 


allEvo = CSV.read("allEvoOutput2.csv", header = false);
h = CSV.read("hawaiiEvoOutput2.csv", header = false);
k = CSV.read("kauaiEvoOutput2.csv", header = false);
kh = CSV.read("kauaiHawaiiEvoOutput2.csv", header = false);
km = CSV.read("kauaiMauiEvoOutput2.csv", header = false);
ko = CSV.read("kauaiOahuEvoOutput2.csv", header = false);
kom = CSV.read("kauaiOahuMauiEvoOutput2.csv", header = false);
m = CSV.read("mauiEvoOutput2.csv", header = false);
mh = CSV.read("mauiHawaiiEvoOutput2.csv", header = false);
o = CSV.read("oahuEvoOutput2.csv", header = false);
oh = CSV.read("oahuHawaiiEvoOutput2.csv", header = false);
om = CSV.read("oahuMauiEvoOutput2.csv", header = false);
omh = CSV.read("oahuMauiHawaiiEvoOutput2.csv", header = false);

# transform all transposed matrices, so calculations go faster downstream

allM = Matrix(allEvo)';
hM = Matrix(h)';
kM = Matrix(k)';
khM = Matrix(kh)';
kmM = Matrix(km)';
koM = Matrix(ko)';
komM = Matrix(kom)';
mM = Matrix(m)';
mhM = Matrix(mh)';
oM = Matrix(o)';
ohM = Matrix(oh)';
omM = Matrix(om)';
omhM = Matrix(omh)'; 


### code from https://discourse.julialang.org/u/rdeits on juliahub 
### see https://discourse.julialang.org/t/improving-performance-of-a-nested-for-loop/29705/7 
# run on "c_cols" ; translated matrix works much better


function triangle_number(n)
    if iseven(n)
        (n ÷ 2) * (n + 1)
    else
        ((n + 1) ÷ 2) * n
    end
end


function distance_matrix4(c)
    cdist = Matrix{eltype(c)}(undef, size(c, 1), triangle_number(size(c, 2) - 1))
    col = 1
    for j in 1:(size(c, 2) - 1)
        for h in (j + 1):size(c, 2)
            for k in 1:size(c, 1)
                cdist[k, col] = abs(c[k, j] - c[k, h])
            end
            col += 1
        end
    end
    return cdist
end



#note that below the matrices are transposed before they're read into the _dist.csv files

allM_dist = distance_matrix4(allM)'; 
hM_dist = distance_matrix4(hM)'; 
kM_dist = distance_matrix4(kM)'; 
khM_dist = distance_matrix4(khM)'; 
kmM_dist = distance_matrix4(kmM)'; 
koM_dist = distance_matrix4(koM)'; 
komM_dist = distance_matrix4(komM)'; 
mM_dist = distance_matrix4(mM)'; 
mhM_dist = distance_matrix4(mhM)'; 
oM_dist = distance_matrix4(oM)'; 
ohM_dist = distance_matrix4(ohM)'; 
omM_dist = distance_matrix4(omM)'; 
omhM_dist = distance_matrix4(omhM)'; 

# write out files

CSV.write("all_dists.csv", DataFrame(allM_dist));
CSV.write("h_dists.csv", DataFrame(hM_dist));
CSV.write("k_dists.csv", DataFrame(kM_dist));
CSV.write("kh_dists.csv", DataFrame(khM_dist));
CSV.write("km_dists.csv", DataFrame(kmM_dist));
CSV.write("ko_dists.csv", DataFrame(koM_dist));
CSV.write("kom_dists.csv", DataFrame(komM_dist));
CSV.write("m_dists.csv", DataFrame(mM_dist));
CSV.write("mh_dists.csv", DataFrame(mhM_dist));
CSV.write("o_dists.csv", DataFrame(oM_dist));
CSV.write("oh_dists.csv", DataFrame(ohM_dist));
CSV.write("om_dists.csv", DataFrame(omM_dist));
CSV.write("omh_dists.csv", DataFrame(omhM_dist));

# dance for joy 


