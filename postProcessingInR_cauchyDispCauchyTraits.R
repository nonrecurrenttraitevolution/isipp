### post- processing in R

### so now you have two or three separate tables of interest: the ISIPPjuliaOutput.csv and ISIPPparameterSpace.csv (output from R)

### the ISIPP_preRun_parameterSpace.csv should have all the run data, including runs that may have crashed... this may also be of interest



### https://r4ds.had.co.nz/relational-data.html is a great resource for working with data in R, especially using dplyr

### especially check out the "mutating joins" topic (13.4) using the function "left join"



### First, get rid of all duplicates of "seed" in each dataset, so you're only working with unique runs





### read in data

rSimulationOutput = read.table(file = "ISIPPparameterSpace.csv", header = FALSE, sep = ",", col.names = c("kauaiTimeSteps","oahuTimeSteps","mauiTimeSteps","hawaiiTimeSteps","rangeRadius","populationCohesion","resourceAssimilation","resourceDimensionality","extinctionRate","dispersalMultiplier", "traitEvolutionMultiplier", "evolvabilityLimit" , "kauaiSize","oahuSize","mauiSize","hawaiiSize","islandDistance","totalRichness","kauaiRichness","oahuRichness","mauiRichness","hawaiiRichness","thisSeed","notThisSeed"))



### works best if "thisSeed" and "notThisSeed" have the same names in both tables

jlDistOutput = read.table(file = "ISIPPjuliaOutput.csv", header = FALSE, sep = ",", col.names = c("thisSeed","notThisSeed","allMed","allMin","allMax","allDistsBelow4","hMed","hMin","hMax","hDistsBelow4","kMed","kMin","kMax","kDistsBelow4","khMed","khMin","khMax","khDistsBelow4","kmMed","kmMin","kmMax","kmDistsBelow4","koMed","koMin","koMax","koDistsBelow4","komMed","komMin","komMax","komDistsBelow4","mMed","mMin","mMax","mDistsBelow4","mhMed","mhMin","mhMax","mhDistsBelow4","oMed","oMin","oMax","oDistsBelow4","ohMed","ohMin","ohMax","ohDistsBelow4","omMed","omMin","omMax","omDistsBelow4","omhMed","omhMin","omhMax","omhDistsBelow4","HoutM_distMed","HoutM_distMin","HoutM_distMax","HoutM_distDistsBelow4","MoutO_distMed","MoutO_distMin","MoutO_distMax","MoutO_distDistsBelow4","OoutK_distMed","OoutK_distMin","OoutK_distMax","OoutK_distDistsBelow4","HoutO_distMed","HoutO_distMin","HoutO_distMax","HoutO_distDistsBelow4","HoutK_distMed","HoutK_distMin","HoutK_distMax","HoutK_distDistsBelow4","MoutK_distMed","MoutK_distMin","MoutK_distMax","MoutK_distDistsBelow4"))



# Add subset of data

midRangeSubset = subset(rSimulationOutput, 2 < rSimulationOutput$hawaiiRichness)

midRangeSubset = subset(midRangeSubset, 200 > midRangeSubset$kauaiRichness)





# further subsetting and some initial notes on CART approaches: 

hYes = subset(rSimulationOutput, 3 < rSimulationOutput$hawaiiRichness)

hmYes = subset(hYes, 3 < hYes$mauiRichness)

hmoYes = subset(hmYes, 3 < hmYes$oahuRichness)

hmokYes = subset(hmoYes, 3 < hmoYes$kauaiRichness)



library(dplyr)

rSimulationOutputUnique = distinct(rSimulationOutput, rSimulationOutput$thisSeed, .keep_all = TRUE)



totalTime = rSimulationOutputUnique$kauaiTimeSteps + rSimulationOutputUnique$oahuTimeSteps + rSimulationOutputUnique$mauiTimeSteps + rSimulationOutputUnique$hawaiiTimeSteps



rSimulationOutputUnique = cbind(rSimulationOutputUnique, totalTime)



library(rpart)

library(rpart.plot)



kauaiRichnessCART = rpart(hmokYesUnique$kauaiRichness ~ hmokYesUnique$populationCohesion + hmokYesUnique$evolvabilityLimit + hmokYesUnique$rangeRadius + hmokYesUnique$resourceAssimilation + hmokYesUnique$resourceDimensionality + hmokYesUnique$extinctionRate + hmokYesUnique$dispersalMultiplier + hmokYesUnique$kauaiSize)

rpart.plot(kauaiRichnessCART)



hawaiiRichnessCART = rpart(hmokYesUnique$hawaiiRichness ~ hmokYesUnique$populationCohesion + hmokYesUnique$evolvabilityLimit + hmokYesUnique$rangeRadius + hmokYesUnique$resourceAssimilation + hmokYesUnique$resourceDimensionality + hmokYesUnique$extinctionRate + hmokYesUnique$dispersalMultiplier + hmokYesUnique$hawaiiSize)

rpart.plot(hawaiiRichnessCART)





library(tibble)

rSimulationOutputUnique = as_tibble(rSimulationOutputUnique)

jlDistOutput = as_tibble(jlDistOutput)



### inner join creates a new tibble with all of the columns from each previous tibble

### keeping only the rows for which "thisSeed" and "notThisSeed" occur in each tibble



progressionData = rSimulationOutputUnique %>%

	inner_join(jlDistOutput)

	



prog1 = progressionData$koMax > progressionData$omhMax	

prog2 = progressionData$omMax > progressionData$mhMax

prog3 = progressionData$koMax > progressionData$oMax

prog4 = progressionData$omMax > progressionData$mMax

prog5 = progressionData$mhMax > progressionData$hMax

prog6 = progressionData$koMax > progressionData$mMax

prog7 = progressionData$koMax > progressionData$hMax

prog8 = progressionData$omMax > progressionData$hMax

prog9 = progressionData$koMax > progressionData$mhMax



progSum = prog1 + prog2 + prog3 + prog4 + prog5 + prog6 + prog7 + prog8 + prog9

koNew = progressionData$koDistsBelow4 - (progressionData$kDistsBelow4 + progressionData$oDistsBelow4)
omNew = progressionData$omDistsBelow4 - (progressionData$oDistsBelow4 +  progressionData$mDistsBelow4)
mhNew = progressionData$mhDistsBelow4 - (progressionData$mDistsBelow4 + progressionData$hDistsBelow4)
allNew = progressionData$allDistsBelow4 - (progressionData$kDistsBelow4 + progressionData$oDistsBelow4 + progressionData$mDistsBelow4 + progressionData$hDistsBelow4)

progressionData = cbind(progressionData, progSum, koNew, omNew, mhNew, allNew)



plot(progressionData$progSum ~ progressionData$islandDistance)



summary(lm(progressionData$progSum ~ progressionData$islandDistance))



progSumCART = rpart(progressionData$progSum ~ progressionData$populationCohesion + progressionData$evolvabilityLimit + progressionData$rangeRadius + progressionData$resourceAssimilation + progressionData$resourceDimensionality + progressionData$extinctionRate + progressionData$dispersalMultiplier + progressionData$hawaiiSize + progressionData$totalTime + progressionData$islandDistance)

rpart.plot(progSumCART)



## don't plot like below; rpart.plot stuff is waaaay better

#par(mfrow = c(1,2), xpd = NA)

#plot(progSumCART); text(progSumCART, use.n = TRUE)



progMed1 = progressionData$koMed > progressionData$omhMed	

progMed2 = progressionData$omMed > progressionData$mhMed

progMed3 = progressionData$koMed > progressionData$oMed

progMed4 = progressionData$omMed > progressionData$mMed

progMed5 = progressionData$mhMed > progressionData$hMed

progMed6 = progressionData$koMed > progressionData$mMed

progMed7 = progressionData$koMed > progressionData$hMed

progMed8 = progressionData$omMed > progressionData$hMed

progMed9 = progressionData$koMed > progressionData$mhMed



progMedSum = progMed1 + progMed2 + progMed3 + progMed4 + progMed5 + progMed6 + progMed7 + progMed8 + progMed9



progressionData = cbind(progressionData, progMedSum)



progMedSumCART = rpart(progressionData$progMedSum ~ progressionData$populationCohesion + progressionData$evolvabilityLimit + progressionData$rangeRadius + progressionData$resourceAssimilation + progressionData$resourceDimensionality + progressionData$extinctionRate + progressionData$dispersalMultiplier + progressionData$hawaiiSize + progressionData$totalTime + progressionData$islandDistance)

rpart.plot(progSumCART)



## don't plot like below; rpart.plot stuff is waaaay better

#par(mfrow = c(1,2), xpd = NA)

#plot(progMedSumCART); text(progMedSumCART, use.n = TRUE)



progOut1 = progressionData$HoutM_distMin > progressionData$hMax	

progOut2 = progressionData$MoutO_distMin > progressionData$mMax

progOut3 = progressionData$OoutK_distMin > progressionData$oMax

progOut4 = progressionData$HoutO_distMin > progressionData$hMax

progOut5 = progressionData$HoutO_distMin > progressionData$mMax

progOut6 = progressionData$HoutK_distMin > progressionData$hMax

progOut7 = progressionData$HoutK_distMin > progressionData$mMax

progOut8 = progressionData$HoutK_distMin > progressionData$oMax

progOut9 = progressionData$MoutK_distMin > progressionData$hMax

progOut10 = progressionData$MoutK_distMin > progressionData$mMax

progOut11 = progressionData$MoutK_distMin > progressionData$mhMax

progOut12 = progressionData$MoutK_distMin > progressionData$omhMax

progOut13 = progressionData$OoutK_distMin > progressionData$mhMax

progOut14 = progressionData$OoutK_distMin > progressionData$omhMax

progOut15 = progressionData$MoutO_distMin > progressionData$hMax

progOut16 = progressionData$MoutO_distMin > progressionData$mhMax


progOutSum = progOut1 + progOut2 + progOut3 + progOut4 + progOut5 + progOut6 + progOut7 + progOut8 + progOut9 + progOut10 + progOut11 + progOut12 + progOut13 + progOut14 + progOut15 + progOut16



progressionData = cbind(progressionData, progOutSum) 



plot(progressionData$progMedSum ~ progressionData$rangeRadius)



summary(lm(progressionData$progMedSum ~ progressionData$rangeRadius))



summary(glm(progressionData$progMedSum ~ progressionData$resourceDimensionality + progressionData$evolvabilityLimit))





plot(progressionData$resourceDimensionality ~ progressionData$progMedSum)

abline(lm(progressionData$resourceDimensionality ~ progressionData$progMedSum))

summary(lm(progressionData$resourceDimensionality ~ progressionData$progMedSum))

