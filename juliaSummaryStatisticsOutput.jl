using Statistics
using CSV
using DataFrames

## so from here, need to output the file again, and do text processing to remove the commas and the first zeros (see unix code snippets), and then read the files back in 
allDist = CSV.read("all_dists4.csv", header = false);
hDist = CSV.read("h_dists4.csv", header = false);
kDist = CSV.read("k_dists4.csv", header = false);
khDist = CSV.read("kh_dists4.csv", header = false);
kmDist = CSV.read("km_dists4.csv", header = false);
koDist = CSV.read("ko_dists4.csv", header = false);
komDist = CSV.read("kom_dists4.csv", header = false);
mDist = CSV.read("m_dists4.csv", header = false);
mhDist = CSV.read("mh_dists4.csv", header = false);
oDist = CSV.read("o_dists4.csv", header = false);
ohDist = CSV.read("oh_dists4.csv", header = false);
omDist = CSV.read("om_dists4.csv", header = false);
omhDist = CSV.read("omh_dists4.csv", header = false);

### read in seeds value so I can associate these data with the R output
theseSeeds = CSV.read("seeds.csv", header = true);


#might want to change things to some kind of big number designation? although log10 will probably still work for big numbers

allDistLogInt = trunc.(BigInt, (log10.(Matrix(allDist .+ 1))));

hDistLogInt = trunc.(BigInt, (log10.(Matrix(hDist .+ 1)))); 
 
kDistLogInt = trunc.(BigInt, (log10.(Matrix(kDist .+ 1))));
 
khDistLogInt = trunc.(BigInt, (log10.(Matrix(khDist .+ 1))));

kmDistLogInt = trunc.(BigInt, (log10.(Matrix(kmDist .+ 1))));

koDistLogInt = trunc.(BigInt, (log10.(Matrix(koDist .+ 1))));

komDistLogInt = trunc.(BigInt, (log10.(Matrix(komDist .+ 1))));

mDistLogInt = trunc.(BigInt, (log10.(Matrix(mDist .+ 1))));

mhDistLogInt = trunc.(BigInt, (log10.(Matrix(mhDist .+ 1))));

oDistLogInt = trunc.(BigInt, (log10.(Matrix(oDist .+ 1))));

ohDistLogInt = trunc.(BigInt, (log10.(Matrix(ohDist .+ 1))));

omDistLogInt = trunc.(BigInt, (log10.(Matrix(omDist .+ 1))));

omhDistLogInt = trunc.(BigInt, (log10.(Matrix(omhDist))));

# then the output 


#this output below is the same, but it makes a 13 x 4 array, instead of a 1 x 52 array, which is what we need... 
# so the code needs to be messier
#output = [
#	median(allDistLogInt) minimum(allDistLogInt) maximum(allDistLogInt) count(x -> x < 4, allDistLogInt)
#	median(hDistLogInt) minimum(hDistLogInt) maximum(hDistLogInt) count(x -> x < 4, hDistLogInt)
#	median(kDistLogInt) minimum(kDistLogInt) maximum(kDistLogInt) count(x -> x < 4, kDistLogInt)
#	median(khDistLogInt) minimum(khDistLogInt) maximum(khDistLogInt) count(x -> x < 4, khDistLogInt)
#	median(kmDistLogInt) minimum(kmDistLogInt) maximum(kmDistLogInt) count(x -> x < 4, kmDistLogInt)
#	median(koDistLogInt) minimum(koDistLogInt) maximum(koDistLogInt) count(x -> x < 4, koDistLogInt)
#	median(komDistLogInt) minimum(komDistLogInt) maximum(komDistLogInt) count(x -> x < 4, komDistLogInt)
#	median(mDistLogInt) minimum(mDistLogInt) maximum(mDistLogInt) count(x -> x < 4, mDistLogInt)
#	median(mhDistLogInt) minimum(mhDistLogInt) maximum(mhDistLogInt) count(x -> x < 4, mhDistLogInt)
#	median(oDistLogInt) minimum(oDistLogInt) maximum(oDistLogInt) count(x -> x < 4, oDistLogInt)
#	median(ohDistLogInt) minimum(ohDistLogInt) maximum(ohDistLogInt) count(x -> x < 4, ohDistLogInt)
#	median(omDistLogInt) minimum(omDistLogInt) maximum(omDistLogInt) count(x -> x < 4, omDistLogInt)
#	median(omhDistLogInt) minimum(omhDistLogInt) maximum(omhDistLogInt) count(x -> x < 4, omhDistLogInt)
#	]
#


output = Int[ theseSeeds[1 , 1] theseSeeds[1 , 2] median(allDistLogInt) minimum(allDistLogInt) maximum(allDistLogInt) count(x -> x < 4, allDistLogInt) median(hDistLogInt) minimum(hDistLogInt) maximum(hDistLogInt) count(x -> x < 4, hDistLogInt) median(kDistLogInt) minimum(kDistLogInt) maximum(kDistLogInt) count(x -> x < 4, kDistLogInt) median(khDistLogInt) minimum(khDistLogInt) maximum(khDistLogInt) count(x -> x < 4, khDistLogInt) median(kmDistLogInt) minimum(kmDistLogInt) maximum(kmDistLogInt) count(x -> x < 4, kmDistLogInt) median(koDistLogInt) minimum(koDistLogInt) maximum(koDistLogInt) count(x -> x < 4, koDistLogInt) median(komDistLogInt) minimum(komDistLogInt) maximum(komDistLogInt) count(x -> x < 4, komDistLogInt) median(mDistLogInt) minimum(mDistLogInt) maximum(mDistLogInt) count(x -> x < 4, mDistLogInt) median(mhDistLogInt) minimum(mhDistLogInt) maximum(mhDistLogInt) count(x -> x < 4, mhDistLogInt) median(oDistLogInt) minimum(oDistLogInt) maximum(oDistLogInt) count(x -> x < 4, oDistLogInt) median(ohDistLogInt) minimum(ohDistLogInt) maximum(ohDistLogInt) count(x -> x < 4, ohDistLogInt) median(omDistLogInt) minimum(omDistLogInt) maximum(omDistLogInt) count(x -> x < 4, omDistLogInt) median(omhDistLogInt) minimum(omhDistLogInt) maximum(omhDistLogInt) count(x -> x < 4, omhDistLogInt) ]

CSV.write("ISIPPjuliaOutput.csv", DataFrame(output), append=true)